﻿
namespace CloudLibrary.Application.Models
{
    public class Resources
    {
        public string Name { get; set; }
        public string Services { get; set; }
        public string ResourceType { get; set; }
        public string SupportService { get; set; }
    }
}
