﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CloudLibrary.Application.Models;
using Microsoft.Extensions.Configuration;

namespace CloudLibrary.Application.Services
{
    public class ProviderService : IProviderService

    {
        private readonly IConfiguration _configuration;

        public ProviderService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Create infrastructure and resources directories
        /// </summary>
        /// <returns>List of added infrastructure directory path</returns>
        public IEnumerable<string> CreateInfrastructure(string providerName, List<string> infraNames)
        {
            string providerDir = Path.Combine(GetCloudServicesDirectory(), providerName);

            if (!Directory.Exists(providerDir))
                throw new DirectoryNotFoundException("Provider directory could not be found.");

            foreach (var name in infraNames)
            {
                string path = Path.Combine(providerDir, name);
                CreateDirectory(path);
                CreateResourcesDirectoryAndAttributeFiles(path);

                yield return path;
            };

        }

        /// <summary>
        /// If this provider does not exist, add its directory 
        /// </summary>
        /// <returns>Provider directory path</returns>
        public string CheckAndCreateProvider(string providerName)
        {
            string path = Path.Combine(GetCloudServicesDirectory(), providerName);
            CreateDirectory(path);

            return path;
        }

        /// <summary>
        /// Delete resource directory of requested infrastructure based on the hierarchical structure
        /// </summary>
        public void DeleteResources(string infraName, string providerName)
        {
            string path = Path.Combine(GetCloudServicesDirectory(), providerName, infraName);
            if (!Directory.Exists(path))
                throw new DirectoryNotFoundException("Requested directory could not be found.");

            var directory = new DirectoryInfo(path);

            //Get Virtual Machine Directory Name
            var resources = _configuration.GetSection("resources").Get<List<Resources>>();
            var virtualFolderName = resources.Where(p => p.ResourceType == "Virtual").FirstOrDefault().Name;

            //Delete Virtual Machine directory 
            var virtualDirectory = directory.GetDirectories(virtualFolderName).FirstOrDefault();
            if (virtualDirectory.Exists)
            {
                virtualDirectory.GetFiles().ToList().ForEach(file => file.Delete());
                virtualDirectory.Delete();
            }

            //Delete other resources
            Delete(directory);
        }

        #region Methods

        private string GetCloudServicesDirectory()
        {
            string currentDir = Environment.CurrentDirectory;
            return Directory.GetParent(currentDir).Parent.FullName;
        }

        private void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        private void CreateResourcesDirectoryAndAttributeFiles(string infraPath)
        {

            var infraName = new DirectoryInfo(infraPath).Name;
            var resources = _configuration.GetSection("resources").Get<List<Resources>>();

            resources.ForEach(item =>
            {

                string path = Path.Combine(infraPath, item.Name);
                CreateDirectory(path);

                var attributes = Newtonsoft.Json.JsonConvert.SerializeObject(item);
                File.WriteAllText($"{path}/{infraName}_SERVER.json", attributes);

            });
        }

        private void Delete(DirectoryInfo directory)
        {
            directory.EnumerateDirectories().ToList().ForEach(item =>
                  {
                      Delete(item);

                      item.GetFiles().ToList().ForEach(file => file.Delete());
                      item.Delete();
                  });
        }

        #endregion


    }
}
