﻿using System;
using System.Collections.Generic;

namespace CloudLibrary.Application.Services
{
    public interface IProviderService
    {
        IEnumerable<string> CreateInfrastructure(string providerName, List<string> infraNames);

        string CheckAndCreateProvider(string providerName);

        void DeleteResources(string infraName, string providerName);

    }
}
