using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CloudLibrary.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //Adding "IGS" as a default provider
            var basePath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
            string path = Path.Combine(basePath, "IGS");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            CreateHostBuilder(args).Build().Run();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
