﻿using System.Collections.Generic;
using System.Linq;
using CloudLibrary.Application.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace CloudLibrary.WebApi.Controllers
{
    [Route("api/providers")]
    public class ProviderManagementController : Controller
    {
        private readonly IProviderService providerService;

        public ProviderManagementController(IProviderService providerService)
        {
            this.providerService = providerService;
        }

        [HttpPost("{providerName}")]
        [SwaggerOperation(Summary = "Adding new provider - IGS provider has been added by default beside the project file.")]
        public IActionResult AddProvider(string providerName)
        {
            var result = providerService.CheckAndCreateProvider(providerName);
            return Ok(result);
        }

        [HttpPost("{providerName}/infrastructures")]
        [SwaggerOperation(Summary = "Adding Infrastructures by giving the list of name in request body")]
        public IActionResult AddInfrustructure(string providerName, [FromBody] List<string> infraNames)
        {
            if (!infraNames.Any())
                return BadRequest();

            var result = providerService.CreateInfrastructure(providerName, infraNames);
            return Ok(result);

        }

        [HttpDelete("{providerName}/infrastructures/{infraName}")]
        [SwaggerOperation(Summary = "Removing resources directory")]
        public IActionResult Delete(string providerName, string infraName)
        {
            providerService.DeleteResources(infraName, providerName);
            return Ok();
        }
    }
}
