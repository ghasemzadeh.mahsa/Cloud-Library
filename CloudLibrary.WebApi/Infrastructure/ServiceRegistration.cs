﻿using System;
using CloudLibrary.Application.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CloudLibrary.WebApi.Infrastructure
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services.AddSingleton<IProviderService, ProviderService>();
        }
    }
}
